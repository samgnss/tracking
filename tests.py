from dataclasses import dataclass
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.patches import Ellipse
from matplotlib.collections import PatchCollection
from itertools import combinations

from tracking.filters.particle import smc_phd, particle, mdsmc_phd
from tracking.measurements.passive_measurements import lob, lob_vec, pdoa, tdoa, pdoa_vec
from tracking.measurements.active_measurements import Range, RangeBearing
from tracking.utils.sensors import sensor
from tracking.utils.simulation_utils import tracking_sim, heatmap
from tracking.trackers.euclidean_tracker import euclidean_tracker


def likelihood(weights, std_dev):
    return np.exp(-1*weights**2 / (2*(std_dev**2))) / np.sqrt(2*np.pi*(std_dev**2))


def likelihood2(weights, std_dev):
    _weights = np.sum(weights**2/std_dev, axis=0)
    return np.exp(-1*_weights/2 - np.log(np.pi*np.sqrt(np.prod(std_dev))))


def prob_detect(states, rx):
    rx_loc = rx.get_position()[:2]
    R_0 = 350
    h_bar = 0.00025
    pd = np.asarray(np.linalg.norm(states - rx_loc, axis=1))
    
    if len(pd[pd<=R_0]):
        pd[pd<=R_0] = 1
    
    if len(pd[pd>R_0]):
        pd[pd>R_0] = 1 - (pd[pd>R_0] - R_0)*h_bar
        if len(pd[pd<0]):
            pd[pd<0] = 0
    return pd

# pd = prob_detect(np.array([[10,1000],[1000,1000]]),sensor('active',0,0,10,1000,0,0,0,0))
# print(pd)
# assert False

meas_model = tdoa()

num_sensors = 4
square = [(1500, 1500, 0),
          (-1500, 1500, 0),
          (-1500, -1500, 0),
          (1500, -1500, 0)]
sensors = []
for n in range(num_sensors):
    # x, y, _ = np.random.randint(-5000, 5000, 3)
    x, y, _ = square[n]
    err = 0#np.array([[0], [0]])
    sensors.append(sensor("active", n, err, x, y, 0, 0, 0, 0))

sensor_iter = combinations(sensors, 1)

tx = np.array([[1000, 0, -1000, 3, 0, 0], [-1000, 3, 1000, 0, 0, 0], [-4500, 2, 4000, -2, 0, 0]])

# filt = smc_phd(5000, 2000, meas_model.meas_vec, 100e-9, likelihood, process_noise=1, map_bounds=[10000, 10000, 0], pb=0.01, clutter_inten=0.002)
filt = mdsmc_phd(3000, 3000, meas_model.meas_vec, 100e-9, likelihood, birth_model=meas_model.generator, process_noise=0.05, map_bounds=[10000, 10000, 0], pb=0.0001, clutter_inten=0.01)
sim = tracking_sim(100, sensors, tx, 0, 0.05, meas_model.meas, map_bound=10000, num_sensors=2)
sim.step_all()
track_manager = euclidean_tracker(max_dist=60, variance_dist=400)

fig, ax = plt.subplots()
# _, ax2 = plt.subplots()

track_manager._ax = ax

for i, m in enumerate(sim.meas):
    ax.cla()

    # ax2.cla()
    print("Step: %d" % i)
    # ax.set_xlim([sim.emitter_states[0][i, 0]-100, sim.emitter_states[0][i, 0]+100])
    # ax.set_ylim([sim.emitter_states[0][i, 2]-100, sim.emitter_states[0][i, 2]+100])
    esti_means, esti_covar = filt.step(m)
    if esti_means is None:
        # plt.show()
        ax.plot(filt.states[:,0], filt.states[:,2], 'ob', markersize=5)

        for sen in sim.sensor_states:
            ax.plot(sen[:i+1,0], sen[:i+1,2], 'g^', markersize=5)

        em_x = []
        em_y = []
        l = None
        for emitter in sim.emitter_states:
            ax.plot(emitter[:i+1, 0], emitter[:i+1, 2], '-r*', label=l)
            l = '_nolegend_'
        plt.pause(0.1)
        continue
    ell = filt.get_conf()

    track_manager.add_detections(i, esti_means, esti_covar, ell)

    ax.plot(filt.states[:,0], filt.states[:,2], 'ob', markersize=5)

    em_x = []
    em_y = []
    l = None
    for emitter in sim.emitter_states:
        ax.plot(emitter[:i+1, 0], emitter[:i+1, 2], '-r*', label=l)
        l = '_nolegend_'
    
    ellp = []
    for e, mean, cvar in zip(ell, esti_means, esti_covar):
    #     if np.sqrt(np.trace(cvar)) < 100:
    #         locX.append(mean[0])
    #         locY.append(mean[2])

        ax.plot(mean[0], mean[2], 'g*', markersize=15)
        p = Ellipse(mean[::2], e[0], e[1], np.rad2deg(e[2]))
        ellp.append(p)

    pc = PatchCollection(ellp, facecolor='red', alpha=0.75)
    ax.add_collection(pc)

    track_manager.plot_tracks()

    ax.grid()
    ax.set_xlabel("X (m)")
    ax.set_ylabel("Y (m)")
    ax.set_title(f"Filtered Updates at time: {i}")
    ax.legend(["Particle System", "Sensors", 'Ground truth', "Tracks"])
    plt.pause(0.001)
    fig.savefig(f'examples/figs/plot_{i}.png')
plt.show()
    

# ax.cla()
# for emitter in sim.emitter_states:
#         ax.plot(emitter[:, 0], emitter[:, 2], '-r*')
# track_manager.plot_tracks()

# ax.grid()
# ax.set_xlabel("X (m)")
# ax.set_ylabel("Y (m)")

# # plt.figure()
# # for emitter in sim.emitter_states:
# #         plt.plot(emitter[:, 0], emitter[:, 2], '-r*')

# # plt.plot(locX, locY, 'ob')
# # plt.show()




