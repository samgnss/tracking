import numpy as np

class track_cls:
    def __init__(self, k, state, covar, ell):
        self.k = [k]
        self.state = [state]
        self.covar = [covar]
        self.ell = [ell]

    def dist(self, k, state):
        state_slice = self.state[-1][::2][:2] # only look at 2 dimensions for now
        state_slice2 = state[::2][:2]

        return np.sqrt((state_slice[0] - state_slice2[0])**2 + (state_slice[1] - state_slice2[1])**2)

    def add(self, k, state, covar, ell):
        self.k.append(k)
        self.state.append(state)
        self.covar.append(covar)
        self.ell.append(ell)

class euclidean_tracker:
    def __init__(self, max_dist=10, variance_dist=1000, ax=None):
        self.tracks = []
        self.max_dist = max_dist
        self.variance_dist = variance_dist
        self._ax = ax

    def add_detections(self, k, states, covar, ells):
        for state, co, ell in zip(states, covar, ells):
            if np.sqrt(np.trace(co)) < self.variance_dist:
                if self.tracks:
                    trackIdx = -1
                    minDist = 100e9

                    for n, track in enumerate(self.tracks):
                        dist = track.dist(k, state)
                        if dist < minDist:
                            trackIdx = n
                            minDist = dist

                    if minDist <= self.max_dist:
                        self.tracks[trackIdx].add(k, state, covar, ell)
                    else:
                        print(f"Starting track at time: {k}")
                        self.tracks.append(track_cls(k, state, covar, ell))

                else:
                    print(f"Starting track at time: {k}")
                    self.tracks.append(track_cls(k, state, covar, ell))
        
    def plot_tracks(self):
        
        if self._ax:
            for track in self.tracks:
                x = []
                y = []

                if len(track.state) > 3:
                    for state in track.state:
                        state_slice = state[::2][:2]
                        x.append(state_slice[0])
                        y.append(state_slice[1])

                    self._ax.plot(x, y, '--g', linewidth=3)
