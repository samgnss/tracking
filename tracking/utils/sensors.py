import numpy as np
from dataclasses import dataclass

@dataclass
class sensor:
    """
    Simple sensor container
    """
    sensor_type: str
    sensor_id: int
    meas_error: float
    x: float
    y: float
    z: float
    vx: float
    vy:float
    vz:float

    def get_state(self)->np.array:
        return np.array([self.x, self.vx, self.y, self.vy, self.z, self.vz])

    def set_state(self, state:np.array):
        self.x, self.vx, self.y, self.vy, self.z, self.vz = state
    
    def get_position(self) -> np.array:
        return np.array([self.x, self.y, self.z])

    def set_position(self, pos:np.array):
        self.x, self.y, self.z = pos[0]