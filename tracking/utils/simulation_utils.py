from dataclasses import dataclass

import numpy as np
from scipy.stats import norm, poisson, uniform
import matplotlib.pyplot as plt
from random import shuffle
from ..utils.sensors import sensor
from ..dynamic_models.constant_velocity import linear_constant_velocity
from itertools import combinations
from copy import deepcopy

@dataclass
class meas:
    z: list
    sensors: tuple

class tracking_sim:
    def __init__(self, num_steps, sensors, emitters, clutter_rate, missed_detection_rate, measurement_equation, map_bound=500, num_sensors=1):
        """[summary]
        
        Arguments:
            num_steps {[type]} -- [description]
            sensors {[type]} -- [description]
            emitters {[type]} -- [description]
            clutter_rate {[type]} -- [description]
        """
        self.k = 0 
        self.num_steps = num_steps
        self.sensors = sensors
        self.num_sensors = num_sensors
        self.emitters = emitters
        
        self.clutter_point_dist = uniform(loc=0.5, scale=2*map_bound)
        self.clutter_dist = poisson(clutter_rate)
        self.clutter_gen = lambda : self.clutter_point_dist.rvs((self.clutter_dist.rvs(), 3))

        self.missed_detection_rate = missed_detection_rate
        self.missed_detection_dist = uniform()
        self.missed_detection_gen = lambda num_draws: self.missed_detection_dist.rvs(num_draws) < self.missed_detection_rate

        self.noise_dist = norm()
        
        self.measurement_equation = measurement_equation

        self.meas = num_steps*[None]

        self.F, _ = linear_constant_velocity(1, 0)
        self.emitter_states = np.zeros((len(emitters), num_steps, 6))
        self.sensor_states = np.zeros((len(sensors), num_steps, 6))

    def _gen_measurement(self, k):
        missed_detections = self.missed_detection_gen(len(self.emitters))
        if self.meas[k] is None:
            self.meas[k] = []
        for _sensors in combinations(self.sensors, self.num_sensors):
            print(_sensors)
            z = []
            clutter_points = self.clutter_gen()
            noise = _sensors[0].meas_error

            for emitter, missed_detection in zip(self.emitters, missed_detections):
                if missed_detection:
                    continue
                z.append(self.measurement_equation(emitter[::2], *_sensors) + noise*self.noise_dist.rvs())

            # # add clutter
            for clutter_point in clutter_points:
                z.append(self.measurement_equation(clutter_point, *_sensors) + noise*self.noise_dist.rvs())

            self.meas[k].append(meas(z, deepcopy(tuple(_sensors))))

        # shuffle(self.meas[k])
    def _update_state(self):
        for i, emitter in enumerate(self.emitters):
            self.emitter_states[i, self.k] = emitter
            self.emitters[i] = emitter @ self.F.T
        
        for i, sensor in enumerate(self.sensors): 
            sensor.set_state(sensor.get_state() @ self.F.T)
            self.sensor_states[i, self.k] = sensor.get_state()

    def step(self):
        self._gen_measurement(self.k)
        self._update_state()
        self.k += 1

    def step_all(self):
        for _ in range(self.num_steps):
            self.step()

    def get_measurements(self):
        z = []
        for zk in self.meas:
            if zk is not None:
                for m in zk:
                    z.extend(m.z)
                
        return z

class heatmap:
    def __init__(self, map_bounds, map_scale, measurement_equation):
        self._map_x = np.arange(map_bounds[0], map_bounds[1], map_scale)
        self._map_y = np.arange(map_bounds[0], map_bounds[1], map_scale)
        self._measurement_equation = measurement_equation
        self.map = np.zeros((len(self._map_x), len(self._map_y)))

    def _update_map(self, measurement):
        for i, x in enumerate(self._map_x):
            for j, y in enumerate(self._map_y):
                cell = [x, y, 0]
                self.map[i, j] += (measurement.z - self._measurement_equation(cell, *measurement.sensors))**2

    def step(self, measurements):
        for measurement in measurements:
            self._update_map(measurement)
            self.plot_map()

    def plot_map(self):
        plt.figure()
        plt.pcolormesh(self._map_x, self._map_y, np.exp(-1*self.map / (2*10e-9**2)) / np.sqrt(2*np.pi*10e-9**2))
        plt.show()
        