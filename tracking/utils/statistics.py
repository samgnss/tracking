import numpy as np
import scipy.stats as scistats


def conf_ellipse(covar, conf_level=95):
    """Returns major, minor, and rotation parameters for a confidence ellipse
    See: http://www.visiondummy.com/2014/04/draw-error-ellipse-representing-covariance-matrix/
    for detatils

    Arguments:
        covar {square numpy array} -- Coavariance

    Keyword Arguments:
        conf_level {int} -- Confidence level for the ellipse (default: {95})
    """
    scale = scistats.chi2.isf(1 - conf_level/100., 2)
    vals, vecs = np.linalg.eigh(covar)

    major, minor = 2*np.sqrt(scale*vals)
    rot = np.arctan2(vecs[1][1], vecs[1][0])

    return major, minor, rot


def sample_from_heatmap(map):
    pass


def get_num_effective(weights):
    return 1.0/np.sum(weights**2)

if __name__ == "__main__":
    from sklearn.mixture import GaussianMixture
    import matplotlib.pyplot as plt
    from matplotlib.patches import Ellipse

    guass_ball = np.random.randn(100, 2)  # 3D point cloud

    center = GaussianMixture(1).fit(guass_ball)
    print(center.covariances_)
    x, y, rot = conf_ellipse(center.covariances_[0], 95)

    _, ax = plt.subplots()
    ax.scatter(guass_ball[:, 0], guass_ball[:, 1])

    ell = Ellipse(center.means_[0], x, y, 180*rot /
                  np.pi, alpha=0.5, facecolor='black')

    ax.add_patch(ell)
    plt.show()
