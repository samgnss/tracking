import numpy as np
from ..utils.sensors import sensor


"""
Active (radar) measurments
"""

def position(true_postion, _, noise_std_dev=10):
    """[summary]
    
    Arguments:
        true_postion {[type]} -- [description]
        _ {[type]} -- [description]
    
    Keyword Arguments:
        noise_std_dev {int} -- [description] (default: {10})
    
    Returns:
        [type] -- [description]
    """

    return true_postion + noise_std_dev * np.random.randn()


class Range:
    @staticmethod
    def meas(tx_loc: np.array, rx: sensor, flat=True):
        """
        Range to target
        """
        if flat:
            rx_loc = rx.get_position()[:2]
            tx_loc = tx_loc[:2]
        return np.linalg.norm(tx_loc - rx_loc)

    @staticmethod
    def meas_vec(tx_loc: np.array, rx: sensor, flat=True):
        """Time difference of arrival
        
        Arguments:
            tx_locs {[type]} -- [description]
            rx_pairs {[type]} -- [description]
        
        Keyword Arguments:
            wave_speed {[type]} -- [description] (default: {constants._LIGHT_SPEED})
        """
        if flat:
            rx_loc = rx.get_position()[:2]
            tx_loc = tx_loc[:,:2]
        return  np.linalg.norm(tx_loc - rx_loc, axis=1)

    @staticmethod
    def generator(rx, rng, rng_std_dev, num_samples, flat=True, circularize=True):
        rx_loc = rx.get_position()[:2]
        if not flat:
            raise NotImplementedError('3D range models not supported yet')

        if circularize:
            nu = rng + rng_std_dev*np.random.randn(num_samples, 2)
            t = np.linspace(0, 2*np.pi, num_samples)
            nu[:,0] = nu[:,0]*np.cos(t)+rx_loc[0]
            nu[:,1] = nu[:,1]*np.sin(t)+rx_loc[1]
        else:
            nu = rng + rng_std_dev*np.random.randn(num_samples, 1)
        
        return nu


class Bearing:
    @staticmethod
    def meas(tx_loc: np.array, rx: sensor):
        """[summary]
        
        Arguments:
            tx_loc {np.array} -- Target location [x, y, z] nominally ENU referenced
            rx {sensors.sensor} -- Sensor location
        
        Keyword Arguments:
            northing {bool} -- [description] (default: {False})
        
        Returns:
            tuple -- azimuth and elevation to the target
        """
        translated = tx_loc - rx.get_position()
        az = np.mod(np.arctan2(translated[1], translated[0]) + 2*np.pi, 2*np.pi)
        return az

    @staticmethod
    def meas_vec(tx_loc: np.array, rx: sensor):
        """Time difference of arrival
        
        Arguments:
            tx_locs {[type]} -- [description]
            rx_pairs {[type]} -- [description]
        
        Keyword Arguments:
            wave_speed {[type]} -- [description] (default: {constants._LIGHT_SPEED})
        """
        translated = tx_loc - rx.get_position()
        az = np.mod(np.arctan2(translated[:, 1], translated[:, 0]) + 2*np.pi, 2*np.pi)
        return az

    @staticmethod
    def generator(rx, ang, ang_std_dev, num_samples, flat=True):
        if not flat:
            raise NotImplementedError('3D range models not supported yet')
        
        nu = ang_std_dev*np.random.randn(num_samples, 2)
        nu[:,0] = np.cos(ang+nu[:,0])
        nu[:,1] = np.sin(ang+nu[:,1])
        return nu
    

class RangeBearing:
    @staticmethod
    def meas(tx_loc: np.array, rx: sensor, flat=True):
        rng = Range.meas(tx_loc, rx, flat)
        bear = Bearing.meas(tx_loc, rx)
        return np.array([[rng], [bear]])
    
    @staticmethod
    def meas_vec(tx_loc: np.array, rx: sensor, flat=True):
        rng = Range.meas_vec(tx_loc, rx, flat)
        bear = Bearing.meas_vec(tx_loc, rx)
        return np.vstack((rng, bear))

    @staticmethod
    def generator(rx, Z, std_dev, num_samples, flat=True):
        rx_loc = rx.get_position()[:2]
        range_samples = Range.generator(rx, Z[0], std_dev[0], num_samples, circularize=False)
        angle_samples = Bearing.generator(rx, Z[1], std_dev[1], num_samples)
        
        return range_samples*angle_samples + rx_loc
