"""
passive measurement models
"""

import numpy as np

LIGHT_SPEED = np.float64(299792458)
DEFAULT_GAMMA = 1

def pdoa(tx_loc, rx_1, rx_2, gamma=DEFAULT_GAMMA):
    """Power difference of arrival using R**(\gamma) path loss
    
    Arguments:
        tx_locs {[type]} -- [description]
        rx_pairs {[type]} -- [description]
    
    Keyword Arguments:
        gamma {[type]} -- [description] (default: {constants._DEFAULT_GAMMA})
    """
    dist1 = np.sqrt((tx_loc[0] - rx_1.x)**2 + (tx_loc[1] - rx_1.y)**2 + (tx_loc[2] - rx_1.z)**2)
    dist2 = np.sqrt((tx_loc[0] - rx_2.x)**2 + (tx_loc[1] - rx_2.y)**2 + (tx_loc[2] - rx_2.z)**2)
    return 10*gamma*np.log10(dist2 / dist1)

def pdoa_vec(tx_loc, rx_1, rx_2, gamma=DEFAULT_GAMMA):
    """Power difference of arrival using R**(\gamma) path loss
    
    Arguments:
        tx_locs {[type]} -- [description]
        rx_pairs {[type]} -- [description]
    
    Keyword Arguments:
        gamma {[type]} -- [description] (default: {constants._DEFAULT_GAMMA})
    """
    dist1 = np.sqrt((tx_loc[:, 0] - rx_1.x)**2 + (tx_loc[:, 1] - rx_1.y)**2 + (tx_loc[:, 2] - rx_1.z)**2)
    dist2 = np.sqrt((tx_loc[:, 0] - rx_2.x)**2 + (tx_loc[:, 1] - rx_2.y)**2 + (tx_loc[:, 2] - rx_2.z)**2)
    return 10*gamma*np.log10(dist2 / dist1)


class tdoa:
    @staticmethod
    def meas(tx_loc, rx_1, rx_2, wave_speed=LIGHT_SPEED):
        """Time difference of arrival
        
        Arguments:
            tx_locs {[type]} -- [description]
            rx_pairs {[type]} -- [description]
        
        Keyword Arguments:
            wave_speed {[type]} -- [description] (default: {constants._LIGHT_SPEED})
        """
        toa1 = np.sqrt((tx_loc[0] - rx_1.x)**2 + (tx_loc[1] - rx_1.y)**2)
        toa2 = np.sqrt((tx_loc[0] - rx_2.x)**2 + (tx_loc[1] - rx_2.y)**2)
        return  (toa1 - toa2)/wave_speed

    @staticmethod
    def meas_vec(tx_loc, rx_1, rx_2, wave_speed=LIGHT_SPEED):
        """Time difference of arrival
        
        Arguments:
            tx_locs {[type]} -- [description]
            rx_pairs {[type]} -- [description]
        
        Keyword Arguments:
            wave_speed {[type]} -- [description] (default: {constants._LIGHT_SPEED})
        """
        toa1 = np.sqrt((tx_loc[:, 0] - rx_1.x)**2 + (tx_loc[:, 1] - rx_1.y)**2)
        toa2 = np.sqrt((tx_loc[:, 0] - rx_2.x)**2 + (tx_loc[:, 1] - rx_2.y)**2)
        return  (toa1 - toa2)/wave_speed

    @staticmethod
    def generator(rxs, dt, err, num_samples, bound=10000, wave_speed=LIGHT_SPEED):
        rx1 = rxs[0].get_position()[:2]
        rx2 = rxs[0].get_position()[:2]

        B = np.sqrt((rx2[0] - rx1[0])**2 + (rx2[1] - rx1[1])**2)
        alpha0 = -1*np.arctan2((rx2[1] - rx1[1]), (rx2[0] - rx1[0]))
    
        count = 0
        r1_samples = np.zeros((num_samples,), dtype=np.float64)
        delta12 = np.zeros((num_samples,), dtype=np.float64)

        while count < num_samples:
            delta12[count] = wave_speed * np.random.normal(dt, err)
            r1_samples[count] = np.abs(np.random.uniform((delta12[count] + B) / 2., bound))
            count += 1

        flips = 2.0 * np.random.randint(0, 2, (num_samples,)) - 1.0
        arg = np.clip((delta12**2 - B**2 - 2*delta12*r1_samples) / (2*B*r1_samples), -1, 1)
        alpha = flips*np.arccos(arg)

        angles = np.array([np.cos(alpha - alpha0), np.sin(alpha - alpha0)])

        samples = rx1[:, None] - r1_samples * angles
        
        return samples.T

        
def fdoa(tx_locs, rx_locs):
    """[summary]
    
    Arguments:
        tx_locs {[type]} -- [description]
        rx_locs {[type]} -- [description]
    """
    pass

def lob(tx_loc, rx_loc):
    return np.arctan2(tx_loc[1] - rx_loc.y, tx_loc[0] - rx_loc.x)

def lob_vec(tx_loc, rx_loc):
    return np.arctan2(tx_loc[:, 1] - rx_loc.y, tx_loc[:, 0] - rx_loc.x)

class toa:
    @staticmethod
    def meas(tx_loc, rx, wave_speed=LIGHT_SPEED):
        """Time of arrival
        """
        return np.sqrt((tx_loc[:, 0] - rx.x)**2 + (tx_loc[:, 1] - rx.y)**2 + (tx_loc[:, 2] - rx.z)**2) / wave_speed

    @staticmethod
    def generator(rx_locs, toa, err):
        return

def poa():
    """power of arrival
    """
    pass

