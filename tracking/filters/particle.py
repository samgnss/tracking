import numpy as np
import scipy.stats as stats
from sklearn.mixture import GaussianMixture
from random import shuffle
import matplotlib.pyplot as plt

from .filter_base import _base
from ..dynamic_models.constant_velocity import linear_constant_velocity
from ..utils.statistics import conf_ellipse, get_num_effective



class particle(_base):
    def __init__(self, num_particles, measurement_equation, measurement_std_dev, likelihood, map_bounds=np.array([500, 500, 0]), process_noise=0.0001):
        # [x vx y vy z vz]
        self.states = 2*np.random.rand(num_particles, 6) - 1
        self.states[:, ::2] *= map_bounds
        self.weights = (1.0/num_particles) * np.ones(num_particles, dtype=np.float128)

        self._num_particles = num_particles
        self._std_dev = measurement_std_dev
        self._map_bounds = map_bounds
        self._measurement_equation = measurement_equation
        self._likelihood = likelihood
        self._F, self._Q = linear_constant_velocity(1)
        self._Q *= process_noise
        self.curr_estimates = []
        self.nthr = num_particles/10

    def step(self, measurements):
        self._predict()
        self._update(measurements)

        Neff = get_num_effective(self.weights)
        print(Neff)
        # if Neff < self.nthr:
        self._resample()

        self.curr_estimates = [np.mean(self.states, axis=0)], [
            np.cov(self.states[0:3:2])]
        return self.curr_estimates

    def _predict(self):
        self.states = self.states @ self._F.T
        self.states += np.random.multivariate_normal(
            np.zeros(6), self._Q, self._num_particles)
        self.states[:, 4:] = np.zeros_like(self.states[:, 4:])

    def _update(self, measurements):
        # shuffle(measurements)
        for meas in measurements:
            for z in meas.z:
                error = (z - self._measurement_equation(self.states[:, ::2], *meas.sensors))
                self.weights *= self._likelihood(error, self._std_dev)
                # self.weights/=np.max(self.weights)

    def _resample(self):
        weights_norm = self.weights / np.sum(self.weights)
        weight_cdf = np.cumsum(weights_norm)

        # uniformly sample from the cdf using searchsorted
        idxs = np.searchsorted(weight_cdf, np.random.rand(self._num_particles))

        self.states = self.states[idxs]
        self.weights = (1/self._num_particles) * \
            np.ones((1, self._num_particles))

    def get_conf(self, state_slice=np.s_[:, 0:3:2]):
        ells = []
        if self.curr_estimates is not None:
            for covar in self.curr_estimates[1]:
                print(covar)
                ells.append(conf_ellipse(covar))

        return ells


class smc_phd(_base):
    def __init__(self, num_particles, num_particles_per_target, measurement_equation, measurement_std_dev, likelihood,
                 map_bounds=np.array([500, 500, 0]), process_noise=0.0001, state_estimator=GaussianMixture,
                 pd=0.9, ps=0.8, pb=0.0001, clutter_inten=2):
        # [x vx y vy z vz]
        self.states = 2*np.random.rand(num_particles, 6) - 1
        self.states[:, ::2] *= map_bounds
        self.weights = (1/num_particles) * np.ones(num_particles)
        self._map_bounds = map_bounds

        self._num_particles = num_particles
        self._num_particles_per_target = num_particles_per_target
        self._std_dev = measurement_std_dev
        self._map_bounds = map_bounds
        self._measurement_equation = measurement_equation
        self._likelihood = likelihood
        self._F, self._Q = linear_constant_velocity(1, process_noise)
        self._Q *= process_noise
        self._state_estimator = state_estimator
        self._pd = pd
        self._ps = ps
        self._pb = pb
        self._clutter_inten = clutter_inten*(1.0 / self._map_bounds[0])
        self.curr_estimates = None
        self.nthr = num_particles/10

    def _particle_birth(self):
        birth_states = 25*(2*np.random.rand(self._num_particles, 6) - 1)
        birth_states[:, ::2] /= 25
        birth_states[:, ::2] *= self._map_bounds

        birth_weights = (self._pb/self._num_particles) * \
            np.ones(self._num_particles)

        self.states = np.vstack((self.states, birth_states))
        self.weights = np.append(self._ps*self.weights, birth_weights)

        self.states[:, 4:6] = np.zeros_like(self.states[:, 4:6])

    def _predict(self):
        self.states = self.states @ self._F.T
        self.states += np.random.multivariate_normal(
            np.zeros(6), self._Q, self.states.shape[0])

        # spawn new particles
        self._particle_birth()

    def _update(self, measurements, ax=None):
        for meas in measurements:
            sensors = meas.sensors
            lk = np.zeros_like(self.weights)
            psi_k = np.zeros((len(meas.z), len(self.weights)))
            C_k = np.zeros(len(meas.z))

            for k, z in enumerate(meas.z):
                print(z)
                error = (z - self._measurement_equation(self.states[:, ::2], *sensors))
                psi_k[k, :] = self._pd * \
                    self._likelihood(error, self._std_dev)
                C_k[k] = np.sum(psi_k[k,:] * self.weights)

            for psi, C in zip(psi_k, C_k):
                lk += psi / (self._clutter_inten + C)

            self.weights = (1 - self._pd + lk)*self.weights

    def _resample(self):
        num_targets = int(np.round(self.weights.sum()))
        print(num_targets)
        if num_targets == 0:
            return num_targets
        num_particles = self._num_particles_per_target * num_targets
        weights_norm = self.weights / np.sum(self.weights)
        weight_cdf = np.cumsum(weights_norm)

        # uniformly sample from the cdf using searchsorted
        idxs = np.searchsorted(weight_cdf, np.random.rand(num_particles))

        self.states = self.states[idxs]
        self.weights = (num_targets/num_particles) * np.ones(num_particles, dtype=np.float64)
        return num_targets

    def _state_extraction(self, num_targets):
        estimate = self._state_estimator(num_targets).fit(self.states)
        return estimate.means_, estimate.covariances_

    def step(self, measurements):
        self._predict()
        self._update(measurements)
        num_targets = self._resample()

        print(num_targets)

        if num_targets:
            self.curr_estimates = self._state_extraction(num_targets)
        return self.curr_estimates if self.curr_estimates is not None else (None,None)

    def get_conf(self, state_slice=np.s_[0:3:2, 0:3:2]):
        ells = []
        if self.curr_estimates is not None:
            for covar in self.curr_estimates[1]:
                ells.append(conf_ellipse(covar[state_slice]))

        return ells


class mdsmc_phd(_base):
    def __init__(self, num_particles, num_particles_per_target, measurement_equation, measurement_std_dev, likelihood, birth_model,
                 map_bounds=np.array([500, 500, 0]), process_noise=0.0001, state_estimator=GaussianMixture,
                 pd=lambda states,_: 0.95*np.ones(states.shape[0]), ps=0.98, pb=0.0001, clutter_inten=2):
        """Measurement driven SMC-PHD filter
        
        Arguments:
            num_particles {[type]} -- [description]
            num_particles_per_target {[type]} -- [description]
            measurement_equation {[type]} -- [description]
            measurement_std_dev {[type]} -- [description]
            likelihood {[type]} -- [description]
            birth_model {[type]} -- [description]
        
        Keyword Arguments:
            map_bounds {[type]} -- [description] (default: {np.array([500, 500, 0])})
            process_noise {float} -- [description] (default: {0.0001})
            state_estimator {[type]} -- [description] (default: {GaussianMixture})
            pd {float} -- [description] (default: {0.9})
            ps {float} -- [description] (default: {0.8})
            pb {float} -- [description] (default: {0.0001})
            clutter_inten {int} -- [description] (default: {2})
        """
        # [x vx y vy z vz]
        # self.states = 2*np.random.rand(num_particles, 6) - 1
        self.states = np.array([])#np.zeros((num_particles, 6), dtype=float) 
        # self.states[:, ::2] *= map_bounds
        self.weights = np.array([])
        # self.labels = np.ones_like(self.weights, dtype=np.int)
        self._map_bounds = map_bounds

        self._num_particles = num_particles
        self._num_particles_per_target = num_particles_per_target
        self._std_dev = measurement_std_dev
        self._map_bounds = map_bounds
        self._measurement_equation = measurement_equation
        self._likelihood = likelihood
        self._birth_model = birth_model
        self._F, self._Q = linear_constant_velocity(1, process_noise)
        self._state_estimator = state_estimator
        self._pd = pd
        self._ps = ps
        self._pb = pb
        self._clutter_inten = clutter_inten
        print("Clutter: {}".format(self._clutter_inten))
        self.curr_estimates = None

    def _particle_birth(self, dt, weight, sensors):
        # birth_velocity = np.random.uniform(-5, 5, (self._num_particles,2))
        birth_velocity = np.random.normal(0, 5, (self._num_particles,2))

        birth_pos = self._birth_model(sensors, dt, self._std_dev, self._num_particles)

        birth_states = np.zeros((self._num_particles, 6), dtype=np.float)
        birth_states[:,:3:2] = birth_pos
        birth_states[:,1:4:2] = birth_velocity

        birth_weights = weight*np.ones(self._num_particles)

        if len(self.states):
            self.states = np.vstack((self.states, birth_states))
            self.weights = np.append(self.weights, birth_weights)
            self.labels = np.append(self.labels, np.zeros(self._num_particles, dtype=bool))

        else:
            self.states = birth_states
            self.weights = birth_weights
            self.labels = np.zeros(self._num_particles, dtype=bool)

    def _predict(self):
        if len(self.states):
            self.labels[~self.labels] = np.ones_like(self.labels[~self.labels], dtype=bool)
            self.states[self.labels] = self.states[self.labels] @ self._F.T
            self.states[self.labels] += np.random.multivariate_normal(
                np.zeros(6), self._Q, self.states[self.labels].shape[0])
            self.weights[self.labels] *= self._ps

    def _update(self, measurements, ax):
        for meas in measurements:
            sensors = meas.sensors
            print(sensors)
            if not meas.z:
                continue
            weight = self._pb/(len(meas.z)*self._num_particles)
            for k,z in enumerate(meas.z):
                print(f'Z[{k}]: {z}')
                self._particle_birth(z, weight, sensors)

            # update the weights
            self._update_particles(meas.z, sensors)
            if ax is not None:
                ax.scatter(self.states[self.labels, 0], self.states[self.labels, 2], c='g')
                ax.scatter(self.states[~self.labels, 0], self.states[~self.labels, 2], c='r')
            # plt.show()

    def _update_birth_particles(self, lz):
        _weights = self.weights[~self.labels].copy()
        for l in lz:
            self.weights[~self.labels] += _weights/l
        
    def _update_particles(self, meas, sensors):
        if np.any(self.labels):
            lk = np.zeros_like(self.weights[self.labels])
            psi_k = np.zeros((len(meas), len(self.weights[self.labels])))
            C_k = np.zeros(len(meas))
            _prob_detects = self._pd(self.states[self.labels, :6:2], sensors)

            for k, z in enumerate(meas):
                error = (z - self._measurement_equation(self.states[self.labels, ::2], *sensors))
                psi_k[k, :] = _prob_detects * self._likelihood(error, self._std_dev)
                C_k[k] = self._clutter_inten + np.sum(self.weights[~self.labels]) + np.sum(psi_k[k, :]*self.weights[self.labels])

            for psi, C in zip(psi_k, C_k):
                lk += psi*self.weights[self.labels] / C

            self.weights[self.labels] = (1 - _prob_detects)*self.weights[self.labels] + lk
            self._update_birth_particles(C_k)

    def _resample(self):
        if not np.any(self.labels):
            return 0
        nu_p = self.weights[self.labels].sum()
        num_persistant_samples = int(np.round(nu_p * self._num_particles_per_target))

        nu_b = self.weights[~self.labels].sum()
        num_birth_samples = int(np.round(nu_b * self._num_particles_per_target))
        print(f"num birth samples: {num_birth_samples}")
        print(f"num persistant samples: {num_persistant_samples}")

        if num_persistant_samples > 0:
            persistant_weights = self.weights[self.labels]

            weights_norm = persistant_weights / persistant_weights.sum()
            weight_cdf = np.cumsum(weights_norm)

            idxs = np.searchsorted(weight_cdf, np.random.rand(num_persistant_samples))

            sampled_persistant_states = self.states[self.labels][idxs]
            sampled_persistant_weights = (nu_p / num_persistant_samples) * np.ones(num_persistant_samples)

        else:
            sampled_persistant_states = self.states[self.labels]
            sampled_persistant_weights = self.weights[self.labels]

        if num_birth_samples > 100:
            birth_weights = self.weights[~self.labels]

            weights_norm = birth_weights / birth_weights.sum()
            weight_cdf = np.cumsum(weights_norm)

            idxs = np.searchsorted(weight_cdf, np.random.rand(num_birth_samples))
            sampled_birth_states = self.states[~self.labels][idxs]
            sampled_birth_weights = (nu_b / num_birth_samples) * np.ones(num_birth_samples)

        else:
            sampled_birth_states = self.states[~self.labels]
            sampled_birth_weights = self.weights[~self.labels]

        self.states = np.vstack((sampled_persistant_states, sampled_birth_states))
        self.weights = np.hstack((sampled_persistant_weights, sampled_birth_weights))
        self.labels = np.hstack((np.ones_like(sampled_persistant_weights, dtype=bool), np.zeros_like(sampled_birth_weights, dtype=bool)))

        return int(np.round(nu_p))

    def _state_extraction(self, num_targets):
        estimate = self._state_estimator(num_targets).fit(self.states[self.labels])
        return estimate.means_, estimate.covariances_

    def _get_Neff(self):
        pass

    def step(self, measurements, ax=None):
        self._predict()

        if len(measurements):
            self._update(measurements, ax)
            if ax is not None:
                ax.scatter(self.states[:, 0], self.states[:, 2], c=10*np.log10(self.weights + 1e-12))
            num_targets = self._resample()
        else:
            num_targets = 0

        print(num_targets)
        if num_targets:
            self.curr_estimates = self._state_extraction(num_targets)
            return self.curr_estimates
        else:
            return None, None

    def get_conf(self, state_slice=np.s_[0:3:2, 0:3:2]):
        ells = []
        if self.curr_estimates is not None:
            for covar in self.curr_estimates[1]:
                ells.append(conf_ellipse(covar[state_slice]))

        return ells
