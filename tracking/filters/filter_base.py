from abc import ABCMeta, abstractmethod

class _base(metaclass=ABCMeta):
    @abstractmethod
    def step(self, measurements):
        """Step the algorithm
        """
        pass

    @abstractmethod
    def get_conf(self):
        pass