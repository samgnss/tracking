import numpy as np
import scipy.stats as stats

from .filter_base import _base
from ..dynamic_models.constant_velocity import linear_constant_velocity
from ..utils.statistics import conf_ellipse

class Kalman(_base):
    def __init__(self, measurement_equation, measurement_std_dev, map_bounds=np.array([500, 500, 0]), process_noise=0.0001):
        """Linear Kalman filter
        
        Arguments:
            _base {[type]} -- [description]
        """
        self.state = np.zeros((6,1), dtype=np.float64)
        self.covar = np.max(map_bounds)*np.eye(6)*100

        self._std_dev = measurement_std_dev
        self._map_bounds = map_bounds
        self._measurement_equation = measurement_equation

        self._F, self._Q = linear_constant_velocity(1)
        self._Q *= process_noise

    def step(self, measurements):
        self._predict()
        self._update(measurements)
        return self.state

    def _predict(self):
        self.state = self._F @ self.state 
        self.state += np.random.multivariate_normal(np.zeros(6), self._Q, 1).reshape((6,1))

    def _update(self, measurements):
        for meas in measurements:
            error = meas.z.T - self._measurement_equation @ self.state
            innovation = self._std_dev + self._measurement_equation @ self.covar @ self._measurement_equation.T

            kalman_gain =  self.covar @ self._measurement_equation.T @ np.linalg.inv(innovation)

            print(kalman_gain.shape, error.shape)

            self.state = kalman_gain @ error
            self.covar = (np.eye(6) - kalman_gain @ self._measurement_equation) @ self.covar

    def get_conf(self, state_slice=np.s_[0:3:2, 0:3:2]):
        print(self.covar[state_slice])
        return conf_ellipse(self.covar[state_slice])

class UKF(_base):
    def __init__(self, measurement_equation, measurement_std_dev, map_bounds=np.array([500, 500, 0]), process_noise=0.0001):
        """Linear Kalman filter
        
        Arguments:
            _base {[type]} -- [description]
        """
        self.state = np.zeros((6,1), dtype=np.float64)
        self.covar = np.max(map_bounds)*np.eye(6)*100

        self._std_dev = measurement_std_dev
        self._map_bounds = map_bounds
        self._measurement_equation = measurement_equation

        self._F, self._Q = linear_constant_velocity(1)
        self._Q *= process_noise

    def step(self, measurements):
        self._predict()
        self._update(measurements)
        return self.state

    def _predict(self):
        self.state = self._F @ self.state 
        self.state += np.random.multivariate_normal(np.zeros(6), self._Q, 1).reshape((6,1))

    def _update(self, measurements):
        for meas in measurements:
            error = meas.z.T - self._measurement_equation @ self.state
            innovation = self._std_dev + self._measurement_equation @ self.covar @ self._measurement_equation.T

            kalman_gain =  self.covar @ self._measurement_equation.T @ np.linalg.inv(innovation)

            print(kalman_gain.shape, error.shape)

            self.state = kalman_gain @ error
            self.covar = (np.eye(6) - kalman_gain @ self._measurement_equation) @ self.covar

    def get_conf(self, state_slice=np.s_[0:3:2, 0:3:2]):
        print(self.covar[state_slice])
        return conf_ellipse(self.covar[state_slice])