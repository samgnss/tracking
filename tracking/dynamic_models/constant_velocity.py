import numpy as np

def linear_constant_velocity(dt,q):
    F = np.array([[1., dt, 0, 0, 0, 0],
                  [0, 1, 0, 0, 0, 0],
                  [0, 0, 1, dt, 0, 0],
                  [0, 0, 0, 1, 0, 0],
                  [0, 0, 0, 0, 1, dt],
                  [0, 0, 0, 0, 0, 1]])
    
    Q = np.kron(np.eye(3),q*np.array([[dt**3/3,dt**2/2],[dt**2/2, dt]]))

    return F, Q