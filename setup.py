from setuptools import find_packages, setup

print(find_packages("tracking/"))

setup(name='tracking',
      version='0.0.0',
      description='Python RFS multi target tracking library',
      author='Sam Schmidt',
      packages=['tracking',
                'tracking/dynamic_models',
                'tracking/filters',
                'tracking/measurements',
                'tracking/trackers',
                'tracking/utils'],
)